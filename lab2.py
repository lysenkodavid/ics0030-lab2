#!/usr/bin/env python
import numpy as np
from sklearn.datasets import load_boston
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor

import common.feature_selection as feat_sel
import common.test_env as test_env


def print_metrics(y_true, y_pred, label):
    from sklearn.metrics import r2_score
    # Feel free to extend it with additional metrics from sklearn.metrics
    print('%s R squared: %.2f' % (label, r2_score(y_true, y_pred)))


def linear_regression(X, y, print_text='Linear regression all in'):
    # Split train test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    # Linear regression all in
    reg = LinearRegression()
    reg.fit(X_train, y_train)
    print_metrics(y_test, reg.predict(X_test), print_text)
    return reg


def linear_regression_selection(X, y):
    X_sel = feat_sel.backward_elimination(X, y)
    return linear_regression(X_sel, y, print_text='Linear regression with feature selection')


def polynomial_regression(X, y):
    poly_reg = PolynomialFeatures(degree=2)
    X_poly = poly_reg.fit_transform(X)
    lin_reg = linear_regression(X_poly, y, print_text="Polynomial regression")

    return lin_reg


def svr(X, y):
    old_y_shape = y.shape
    sc = StandardScaler()
    X = sc.fit_transform(X)
    y = sc.fit_transform(np.expand_dims(y, axis=1))
    y.shape = old_y_shape

    # For some reason, the shape of the `y` ndarray gets converted from (506,) to (506,1) after scaling, which throws a
    # warning for me when training the regression. I save the old shape of `y` and then set it back to the old shape
    # after scaling. This fixes the issue, and seems to not affect the result at all.

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    reg = SVR(kernel='rbf', gamma='auto')
    reg.fit(X_train, y_train)

    print_metrics(np.squeeze(y_test), np.squeeze(reg.predict(X_test)), 'SVR')


def decision_tree_regression(X, y):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    reg = DecisionTreeRegressor(max_depth=None,
                                min_samples_split=2,
                                min_samples_leaf=1)
    reg.fit(X_train, y_train)

    print_metrics(y_test, reg.predict(X_test), "Decision tree regression")


def random_forest_regression(X, y):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    reg = RandomForestRegressor(n_estimators=10)
    reg.fit(X_train, y_train)

    print_metrics(y_test, reg.predict(X_test), "Random forest regression")


if __name__ == '__main__':
    REQUIRED = ['numpy', 'statsmodels', 'sklearn']
    test_env.versions(REQUIRED)

    # https://scikit-learn.org/stable/datasets/index.html#boston-house-prices-dataset
    X, y = load_boston(return_X_y=True)

    linear_regression(X, y)
    linear_regression_selection(X, y)

    polynomial_regression(X, y)
    svr(X, y)
    decision_tree_regression(X, y)
    random_forest_regression(X, y)

    print('Done')
